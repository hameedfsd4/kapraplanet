﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kapraplanet.web.Startup))]
namespace Kapraplanet.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
